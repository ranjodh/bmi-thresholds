using Pkg, DataFrames, StatFiles, Statistics, Printf, Base.Threads, DataFramesMeta, LinearAlgebra, Combinatorics, Revise, TimerOutputs, Dates, Distributions

#Function to create dummy variables (e.g., for the waves)
function dummy_vars(dataframe::DataFrame, df_variable::Symbol, dummy_prefix::String, print_it::String)
   
    dummy_names = Symbol[]
    for c in minimum(dataframe[:, df_variable]):maximum(dataframe[:, df_variable])
        if print_it == "yes"   
                println(c)
        end 
        dataframe[:, Symbol(dummy_prefix,"_",Int(c))] = (dataframe[:,df_variable] .== c)
        push!(dummy_names, Symbol(dummy_prefix, "_", Int(c)))
    end
    return dataframe, dummy_names 
end


function Within(X::AbstractArray, cum_sum::Vector{Int64})
    
    Wn = zeros(size(X,1),size(X,2))

    Threads.@threads for i in 1:size(cum_sum, 1)-1
        Wn[cum_sum[i]:cum_sum[i+1]-1, :] = X[cum_sum[i]:cum_sum[i+1]-1, :] .- mean(X[cum_sum[i]:cum_sum[i+1]-1, :], dims=1)
    end

    return Wn
end

function remove_rows_greater_than_thresholds(dataframe::DataFrame, 
    variable_thresholds::Dict{Symbol, Int64})

    filter_condition = trues(size(dataframe, 1))  # Initialize filter_condition
    
    for (variable, threshold) in variable_thresholds
        variable_str = string(variable)
        
        if variable_str in names(dataframe)
            filter_condition .= filter_condition .& (abs.(dataframe[!, variable_str]) .<= threshold)
        end
    end
    
    # Return a new DataFrame with rows satisfying the filter_condition
    #return dataframe[filter_condition, :]
    dataframe = dataframe[filter_condition, :]

    exclude_var(varname) = !(startswith(string(varname), "Wn_") || endswith(string(varname), "_mean") || varname == "nrow")
    include_cols = filter(exclude_var, names(dataframe))
    dataframe = select(dataframe, include_cols)

    return dataframe

end

function remove_rows_and_individuals_greater_than_thresholds(dataframe::DataFrame, 
    variable_thresholds::Dict{Symbol, Int64}, id_variable::Symbol)

    # Initialize filter_condition with all `true` values
    filter_condition = trues(size(dataframe, 1))

    # Iterate over each variable and update the filter_condition
    for (variable, threshold) in variable_thresholds
        variable_str = string(variable)
        
        if variable_str in propertynames(dataframe)
            condition = abs.(dataframe[!, variable_str]) .< threshold
            filter_condition .&= condition
        end
    end
    
    # Filter out rows that violate the conditions
    filtered_dataframe = dataframe[filter_condition, :]

    # Create a set of IDs for individuals whose rows should be removed
    ids_to_remove = Set{Any}()

    # Iterate over the unique IDs to identify individuals to remove
    for id in unique(filtered_dataframe[!, id_variable])
        subgroup = filter(row -> row[id_variable] == id, filtered_dataframe)
        if any(subgroup) && all(!filter_condition[subgroup])
            push!(ids_to_remove, id)
        end
    end

    # Filter out rows for the identified individuals
    filtered_dataframe = filter(row -> !(row[id_variable] in ids_to_remove), filtered_dataframe)

    exclude_var(varname) = !(startswith(string(varname), "Wn_") || endswith(string(varname), "_mean") || varname == "nrow")
    include_cols = filter(exclude_var, propertynames(filtered_dataframe))
    filtered_dataframe = select(filtered_dataframe, include_cols)

    return filtered_dataframe
end


#Create time means ("variable"_mean) and Within transformed (Wn_"variable") variables; based on panel dataframe with an id variable 
function add_Withins_and_means(dataframe::DataFrame, id_variable::Symbol, exclude_vars::Vector{String} = String[])

    all_variables = names(dataframe)
    mean_names = setdiff(all_variables, exclude_vars)
    mean_names_symbols = Symbol.(mean_names)
    df = transform( groupby(dataframe, id_variable), mean_names_symbols .=> (x-> x.-mean(x)) .=> [Symbol("Wn_"*String(s)) for s in mean_names_symbols],mean_names_symbols .=> mean)

    return df
end

#Function for data-prepping 
function data_prep(
    dataframe::DataFrame,
    id_name::Symbol,
    spline_name::Symbol,
    y_name::Symbol,
    X_name::Vector{Symbol},
    trim_range::Tuple{Int64, Int64},
    Wn_maximums::Dict{Symbol, Int64}
)

    if hasproperty(dataframe, :select_var)
        selected_rows   = dataframe.select_var .== true
        dataframe       = dataframe[selected_rows, :]  # Select rows where condition is true
    end

    dataframe       = dataframe[(dataframe[:, spline_name] .> trim_range[1]) .& (dataframe[:, spline_name] .< trim_range[2]), :]
    dataframe       = dropmissing(select(dataframe, [id_name, spline_name, y_name]..., X_name))
    dataframe       = transform( groupby(dataframe, id_name), nrow)
    dataframe       = dataframe[(dataframe[:, :nrow] .> 1), :]


    df_COUNTS       = combine( groupby(dataframe, id_name), nrow)
    counts          = df_COUNTS[!, :nrow]
    cum_sum         = cumsum(vcat(1,counts))
    exclude_vars    = [string(id_name), "nrow"]  #add any extra if necessary here
    dataframe       = add_Withins_and_means(dataframe, id_name, exclude_vars)

    initial_size    = size(dataframe,1)
    while true

        #check Within dataset
        dataframe       = remove_rows_greater_than_thresholds(dataframe, Wn_maximums)
        #dataframe       = remove_individuals_with_greater_than_thresholds(dataframe, id_name, Wn_maximums)

        # re-do data prep
        dataframe       = transform( groupby(dataframe, id_name), nrow)
        dataframe       = dataframe[(dataframe[:, :nrow] .> 1), :]

        df_COUNTS       = combine( groupby(dataframe, id_name), nrow)
        counts          = df_COUNTS[!, :nrow]
        cum_sum         = cumsum(vcat(1,counts))
        exclude_vars    = [string(id_name), "nrow"]  #add any extra if necessary here
        dataframe       = add_Withins_and_means(dataframe, id_name, exclude_vars)
   
        if size(dataframe) == initial_size
            break  # Exit the loop if the size didn't change
        else
            initial_size = size(dataframe)  # Update the initial size for the next iteration
        end
        
    end
    
    return dataframe, cum_sum, size(counts,1)

end


#function search_values(search_range::Vector,step::Number,spline::Vector{<:Real},additionals::Vector = [-999])
function search_values(search_range::Vector, step::Number, spline::AbstractVector, additionals::Vector = [-999])

    if size(spline,1) == size(spline,2)
        search_vec = collect(search_range[1]:step:search_range[2])
    else
        quantiles   = quantile(sort(spline),collect(step:step:1-step);sorted=true)
        search_vec  = quantiles[findall(x -> search_range[1] <= x <= search_range[2], quantiles)]
    end
    if additionals ≠ [-999] 
        search_vec = vcat(additionals, search_vec)
    end
    search_vec = vec(search_vec)
    unique!(sort!(search_vec))
    n_threads = size(search_vec)

    return search_vec, n_threads
    
end

function Wn_names(y_name::Symbol, X_name::Vector{Symbol}, spline_name::Union{Symbol, Vector{Symbol}})
    Wny_name = Symbol("Wn_" * string(y_name))
    
    if isa(spline_name, Symbol)
        WnX_name = [Symbol("Wn_" * string(name)) for name in X_name]
        push!(WnX_name, Symbol("Wn_" * string(spline_name)))
    else
        WnX_name = vcat([Symbol("Wn_" * string(name)) for name in X_name], [Symbol("Wn_" * string(name)) for name in spline_name])
    end

    return Wny_name, WnX_name
end

function ols(y_var::Symbol, X_vars::Vector{Symbol}, constant::String, df_correction::Int, df::DataFrame)
    y = df[:, y_var]
    X = Matrix(df[:, X_vars])
    if constant == "yes"
        X = hcat(ones(size(y, 1)), X)
    end

    β = X \ y
    if any(isnan, β) || any(isinf, β)
        β = (X'X) \ X'y
    end

    resid_OLS = y - (X*β)
    SSE = sum(resid_OLS .^ 2)
    df_OLS = size(X, 1) - size(X, 2) - df_correction
    σ² = (SSE / df_OLS)

    stderr_OLS = sqrt.(diag(σ² .* inv(X'X)))
    if any(isnan, stderr_OLS) || any(isinf, stderr_OLS)
        stderr_OLS = sqrt.(abs.(diag(σ² .* inv(X'X))))
    end

    β_all       = hcat(β, stderr_OLS, abs.(β ./ stderr_OLS))
    logL        = -(size(y, 1) / 2) * (1 + log(2π) + log(SSE / size(y, 1)))
    BIC         = (-2*logL) + (log(size(y, 1))*(size(X, 2)+df_correction));

    return β_all, σ², SSE, logL, BIC
end

# Accepts a vector to be splined, x_spline, with breakpoints, gama
# Returns a matrix with the tied splined variables
function spline_x(x_spline::Vector, gama::Vector{Float64})

    if size(gama,2)==1;
        gama=gama'
    end
    gam_mat         = repeat(gama, size(x_spline, 1), 1)
    x_minus         = x_spline .- gam_mat
    dummies         = x_spline .>= gam_mat
    x_splined       = hcat(x_spline, x_minus .* dummies)

    return x_splined
end

# Accepts a panel id vector, a vector to be splined, x, with breakpoints, gama
# Returns a matrix with the tied Within transformed splined variables
function Wn_spline_x(id::Vector{T},x_spline::Vector{T}, gama::Vector{Float64}, cum_sum::Vector{Int64}) where {T}

    x_splined       = spline_x(x_spline, gama)
    spline_matrix   = Within(x_splined, cum_sum)

    return spline_matrix
end

# Function which accepts a data matrix (must be order: id, spline variable, Wy, WX), breakpoints (gama), and panel number of individuals, N, and the cumulative sum vector
# Returns the respective BIC value for this/these breakpoints  
function BIC_i(data::Matrix, gama::Vector{Float64}, N::Number, cum_sum::Vector{Int64})
    
    id          = data[:,1]
    x_spline    = data[:,2]
    Wy          = data[:,3]
    WX          = data[:,4:size(data,2)] 

    spline_matrix = Wn_spline_x(id,x_spline,gama,cum_sum)
    
    if size(gama,2)==1;
        gama=gama'
    end

    WX          = Matrix(hcat(WX,spline_matrix)) 

    β           = WX \ Wy    
    SSE         = sum((Wy - (WX*β)) .^ 2)
    logL        = -(size(Wy, 1) / 2) * (1 + log(2π) + log(SSE / size(Wy, 1)))
    BIC         = -2 * logL + log(size(Wy, 1)) * (size(WX, 2) + N)
    BIC_gama    = [BIC gama]

    return BIC_gama
end

function unique_combinations(search_vec::Vector, K::Integer)
    combs = combinations(search_vec,K) |> collect
    return combs
end

function estimate_threshold_vec(data::Matrix, search_vec::Vector, K::Number, print_it::String, N::Number, cum_sum::Vector{Int64})

    if print_it == "yes"
        println("max breaks(K) = $K")
    end
    
    BIC_opt = Vector{Vector{Float64}}(undef, K)
    
    Threads.@threads for k in 1:K
        search_mat = unique_combinations(search_vec, k)
        num_combinations = size(search_mat, 1)
        if print_it == "yes"
            println("$k, $K")
        end
        BIC_results = zeros(num_combinations, k + 1)
        
        # Perform calculations and store results in BIC_results
        Threads.@threads for i in 1:num_combinations
            BIC_results[i, :] = BIC_i(data, search_mat[i][:], N, cum_sum)
        end
    
        # Sort BIC_results based on the first column
        sort_indices = sortperm(BIC_results[:, 1])
        BIC_results = BIC_results[sort_indices, :]

        # Store the optimal result from BIC_results in BIC_opt
        BIC_opt[k] = BIC_results[1, :]

    end

    return BIC_opt
end

function define_search_mat(search_vec::Vector{T}, K::Number) where {T}
    search_mat = []
    Threads.@threads for k in 1:K
        combs = unique(combinations(search_vec, k))
        append!(search_mat, collect(combs))
    end
    return search_mat
end

function search_matrix_loop(search_vec::Vector{T}, K::Number) where {T}
    if K == 1
        search_mat = search_vec
    else
        if K >= 2
            search_mat = Matrix{Float64}(undef, 0, 2)  # Initialize an empty 2-column matrix
            for i in 1:size(search_vec,1)
                search_mat = vcat(search_mat, hcat(search_vec[i] .* ones(size(search_vec,1) - i + 1, 1), search_vec[i:end]))
            end

            if K >= 3
            
            search_mat = hcat(search_mat, search_mat[:, 2])
            num_combinations = nchoosek(size(search_vec, 1), 3)
            search_mat_3 = zeros(num_combinations, 3)
            start_row = 1

            for i in 1:(size(search_vec,1) - 2)
                for j in (i + 1):(size(search_vec,1) - 1)
                    current_len = size(search_vec,1) - j
                    col_1 = search_vec[i] .* ones(current_len)
                    col_2 = search_vec[j] .* ones(current_len)
                    col_3 = search_vec[j + 1:end]
                    search_mat_3[start_row:(start_row + current_len - 1), :] = hcat(col_1, col_2, col_3)
                    start_row += current_len
                end
            end

            search_mat = vcat(search_mat, search_mat_3)

            if K ≥ 4
                
                search_mat = hcat(search_mat, search_mat[:, 3])
                num_combinations = nchoosek(size(search_vec,1), 4)
                search_mat_4 = zeros(num_combinations, 4)
                start_row = 1
            
                for i in 1:(size(search_vec,1) - 3)
                    for j in (i + 1):(size(search_vec,1) - 2)
                        for k in (j + 1):(size(search_vec,1) - 1)
                            current_len = size(search_vec,1) - k
                            col_1 = search_vec[i] .* ones(current_len)
                            col_2 = search_vec[j] .* ones(current_len)
                            col_3 = search_vec[k] .* ones(current_len)
                            col_4 = search_vec[k + 1:end]
                            search_mat_4[start_row:(start_row + current_len - 1), :] = hcat(col_1, col_2, col_3, col_4)
                            start_row += current_len
                        end
                    end
                end
            
                search_mat = vcat(search_mat, search_mat_4)
            
                if K ≥ 5
                    
                    search_mat = hcat(search_mat, search_mat[:, 4])
                    num_combinations = nchoosek(size(search_vec,1), 5)
                    search_mat_5 = zeros(num_combinations, 5)
                    start_row = 1
                
                    for i in 1:(size(search_vec,1) - 4)
                        for j in (i + 1):(size(search_vec,1) - 3)
                            for k in (j + 1):(size(search_vec,1) - 2)
                                for l in (k + 1):(size(search_vec,1) - 1)
                                    current_len = size(search_vec,1) - l
                                    col_1 = search_vec[i] .* ones(current_len)
                                    col_2 = search_vec[j] .* ones(current_len)
                                    col_3 = search_vec[k] .* ones(current_len)
                                    col_4 = search_vec[l] .* ones(current_len)
                                    col_5 = search_vec[(l + 1):end]
                                    search_mat_5[start_row:(start_row + current_len - 1), :] = hcat(col_1, col_2, col_3, col_4, col_5)
                                    start_row += current_len
                                end
                            end
                        end
                    end
                
                    search_mat = vcat(search_mat, search_mat_5)
                end
            end
        end
    end 
  
end   
    return search_mat
end


function estimate_threshold_mat(data::Matrix, search_mat::Vector{Any}, K::Number, print_it::String, N::Number, cum_sum::Vector{Int64})

    if print_it == "yes"
        println("max breaks(K) = $K")
    end

    BIC_results = Vector{Vector{Float64}}(undef, size(search_mat, 1))

    Threads.@threads for i in 1:size(search_mat, 1)
        if print_it == "yes"
            println("$i, $(size(search_mat, 1))")
        end
    
        # Perform calculations and store results in BIC_results
        result = BIC_i(data, search_mat[i], N, cum_sum)
        BIC_results[i] = vec(result)
    end
        
    # Sort BIC_results based on the first column
    sort_indices    = sortperm(BIC_results[:, 1])
    BIC_results     = BIC_results[sort_indices, :]

    return BIC_results[1, :]
end

# function for printing BIC output
function format_BIC_opt(BIC_opt)
    formatted_output = []
    for item in BIC_opt
        # Check if item is a vector, and format the first element as a regular floating-point number
        if isa(item, Vector{Float64})
            formatted_item = join([@sprintf("%.1f", item[1]), item[2:end]...], ", ")
        else
            formatted_item = join([@sprintf("%s", val) for val in item], ", ")
        end
        push!(formatted_output, formatted_item)
    end
    return join(formatted_output, "\n")
end





function get_result_title(BIC_title::String, BICs::Matrix{Float64})
    #BIC_title = "BIC and breakpoint(s):"
    s = @sprintf("%20s\n", BIC_title)
    if size(BICs,2) == 2
        s *= @sprintf("%-20s %7.0f %4.0f\n","BIC: breaks:", BICs[1], BICs[2])
    elseif size(BICs,2) == 3
        s *= @sprintf("%-20s %7.0f %4.1f %4.1f\n", "BIC: breaks:", BICs[1], BICs[2], BICs[3])
    elseif size(BICs,2) == 4
        s *= @sprintf("%-20s %7.0f %4.1f %4.1f %4.1f\n", "BIC: breaks:", BICs[1], BICs[2], BICs[3], BICs[4])
    elseif size(BICs,2) == 5
        s *= @sprintf("%-20s %7.0f %4.1f %4.1f %4.1f %4.1f\n", "BIC: breaks:", BICs[1], BICs[2], BICs[3], BICs[4], BICs[5])
    elseif size(BICs,2) == 6
        s *= @sprintf("%-20s %7.0f %4.1f %4.1f %4.1f %4.1f %4.1f\n", "BIC: breaks:", BICs[1], BICs[2], BICs[3], BICs[4], BICs[5], BICs[6])
    end
    return s
end

function getresult_table(results_table::Matrix{Float64},var_names::Vector{SubString{String}})
    s = @sprintf("%16s  %8s  (%4s)  %4s\n", "", "Coeff", "s.e.", "t-stat")
    for (var, result) in zip(var_names, eachrow(results_table))
        temp = @sprintf("%-18s %8.4f (%5.3f) %5.2f\n", var, result[1], result[2], result[3])
        s = s * temp
    end
    return s
end

# Define a df for printing
function print_df_DSTATS(dataframe,include_Wns_DSTATS::String)

    if include_Wns_DSTATS == "no"
        exclude_var(varname) = !(startswith(string(varname), "Wn_") || endswith(string(varname), "_mean"))
        include_cols = filter(exclude_var, names(dataframe))
        df_DSTATS = select(dataframe, include_cols)
    else
        df_DSTATS = dataframe
    end
    summary_stats = describe(df_DSTATS, :mean, :std, :min, :q25, :median, :q75, :max, :nmissing)
    summary_stats = @sprintf("%s", summary_stats)
    return summary_stats
end

