proc 1 = _panel_data_prep_spline(fname,id,wave,y,X,x_spline,y_name,X_names,spline_name,spline_cut_points,Wn_cut_points); 
    local data, del_spline, idcontig, t1, fh, all_names, ret, Wn_spline, Wn_del, count, data_trim, Wn_spline_name;

    if cols(X) ne rows(X_names);
        errorlog "cols(X) ne X_names";
        stop;
    endif;

    all_names           = "id_contig" $| "wave_contig" $| y_name $| X_names $| spline_name;
    data	            = packr(id~wave~y~X~x_spline);
    data	            = sortmc(data,1|2);
    
    x_spline            = data[.,cols(data)];
    del_spline          = (x_spline .le spline_cut_points[1]) + (x_spline .ge spline_cut_points[2]);
    data_trim           = delif(data,del_spline);
    {idcontig,t1,count} = _id_contiguous(data_trim[.,1]);

    data_trim           = delif(data_trim,t1 .eq 1);
    {idcontig,t1,count} = _id_contiguous(data_trim[.,1]);
    
    {Wn_spline}	        = Within(data_trim[.,cols(data_trim)],count);
    
    do until minc(Wn_spline) ge (-Wn_cut_points) and maxc(Wn_spline) le Wn_cut_points;
        
        Wn_del              = abs(Wn_spline) .ge Wn_cut_points;
        data_trim           = delif(data_trim,Wn_del);
        {idcontig,t1,count} = _id_contiguous(data_trim[.,1]);
        data_trim           = delif(data_trim,t1 .eq 1);
        {idcontig,t1,count} = _id_contiguous(data_trim[.,1]);
        {Wn_spline}	        = Within(data_trim[.,cols(data_trim)],count);
                
    endo;
    
    data_trim           = data_trim~Wn_spline;
    data_trim	        = sortmc(data_trim,1|2);
    {idcontig,t1,count} = _id_contiguous(data_trim[.,1]);
    data_trim           = delif(data_trim,t1 .eq 1);
    {idcontig,t1,count} = _id_contiguous(data_trim[.,1]);
    data_trim[.,1]      = idcontig;
    
    Wn_spline_name      = "Wn_"$+spline_name;
    all_names           = all_names $| Wn_spline_name;
    
    fh                  = dataCreate(fname, all_names, rows(all_names), 8, 1);
    ret                 = writer(fh, data_trim);
    fh                  = close(fh);
    
retp(count);
endp;


proc 1 = _search_mat(capK,search_vec); 
    local search_mat, search_mat_3, search_mat_4, search_mat_5, i, j, k, l, col_1, col_2, col_3, col_4, col_5, start_row, current_len;

    search_mat = {};

    if capK eq 1;
    
        search_mat = search_vec;

    else;

        if capK ge 2;
    
            for i (1,rows(search_vec),1);
                search_mat = search_mat| 
                    ((search_vec[i] .*. ones(rows(search_vec)-i+1,1))~search_vec[i:rows(search_vec)]);
            endfor;

            if capK ge 3;  
        
                search_mat  = search_mat~search_mat[.,2]; //this contains all singles and doubles 
            
                search_mat_3 = zeros(numCombinations(rows(search_vec),3),3);
                start_row = 1;
                for i (1,rows(search_vec)-2,1);
                    for j (i+1,rows(search_vec)-1,1);
            
                        current_len = rows(search_vec)-j;
                        col_1       = (search_vec[i] .*. ones(rows(search_vec)-j,1));
                        col_2       = (search_vec[j] .*. ones(rows(search_vec)-j,1));
                        col_3       = (search_vec[j+1:rows(search_vec)]);    
                        search_mat_3[start_row:start_row+current_len-1,.] = (col_1~col_2~col_3);
                        start_row = start_row + current_len;
                    
                    endfor;
                endfor;
            
                search_mat = search_mat|search_mat_3;

                if capK ge 4;

                    search_mat  = search_mat~search_mat[.,3]; //this contains all singles...trebles

                    search_mat_4 = zeros(numCombinations(rows(search_vec),4),4);
                    start_row = 1;
                    for i (1,rows(search_vec)-3,1);
                        for j (i+1,rows(search_vec)-2,1);
                            for k (j+1,rows(search_vec)-1,1);
            
                                current_len = rows(search_vec)-k;
                                col_1           = (search_vec[i] .*. ones(rows(search_vec)-k,1));
                                col_2           = (search_vec[j] .*. ones(rows(search_vec)-k,1));
                                col_3           = (search_vec[k] .*. ones(rows(search_vec)-k,1));
                                col_4           = (search_vec[k+1:rows(search_vec)]);     
                                search_mat_4[start_row:start_row+current_len-1,.] = (col_1~col_2~col_3~col_4);
                                start_row = start_row + current_len;
                            
                            endfor;            
                        endfor;
                    endfor;

                    search_mat = search_mat|search_mat_4;

                    if capK ge 5;

                        search_mat  = search_mat~search_mat[.,4]; //this contains all singles...quadruples

                        search_mat_5 = zeros(numCombinations(rows(search_vec),5),5);
                        start_row = 1;
                        for i (1,rows(search_vec)-4,1);
                            for j (i+1,rows(search_vec)-3,1);
                                for k (j+1,rows(search_vec)-2,1);
                                    for l (k+1,rows(search_vec)-1,1);
            
                                        current_len = rows(search_vec)-l;
                                        col_1           = (search_vec[i] .*. ones(rows(search_vec)-l,1));
                                        col_2           = (search_vec[j] .*. ones(rows(search_vec)-l,1));
                                        col_3           = (search_vec[k] .*. ones(rows(search_vec)-l,1));
                                        col_4           = (search_vec[l] .*. ones(rows(search_vec)-l,1));
                                        col_5           = (search_vec[l+1:rows(search_vec)]);     
                                        search_mat_5[start_row:start_row+current_len-1,.] = (col_1~col_2~col_3~col_4~col_5);
                                        start_row = start_row + current_len;
                                    
                                    endfor;
                                endfor;            
                            endfor;
                        endfor;

                        search_mat = search_mat|search_mat_5;

                    endif;
                endif;
            endif;
        endif;
    endif;

retp(search_mat);
endp;


proc 3 = _id_contiguous(id);
    local idcontig,irep,t1,count,count_sum;
    
    idcontig = 1;
    irep = 2;
    do while irep le rows(id);
        if id[irep] eq id[irep-1];
            idcontig = idcontig|idcontig[irep-1];
        else;
            idcontig = idcontig|(idcontig[irep-1]+1);
        endif;
        irep = irep + 1;
    endo;

    count       = counts(idcontig,seqa(minc(idcontig),1,maxc(idcontig)-minc(idcontig)+1));
    count_sum   = cumsumc(1|count);

    t1 = zeros(rows(idcontig),1);
    
    threadfor irep(1,rows(count_sum)-1,1);       
        t1[count_sum[irep]:count_sum[irep+1]-1,.]	= count[irep];     
    threadendfor;

retp(idcontig,t1,count);
endp;

proc 1 = Within(X,count);
    local count_sum, Wn_X;
    
    count_sum   = cumsumc(1|count);
    Wn_X        = zeros(rows(X),cols(X));
    
    threadfor irep(1,rows(count_sum)-1,1);
        Wn_X[count_sum[irep]:count_sum[irep+1]-1,.]	= X[count_sum[irep]:count_sum[irep+1]-1,.] - meanc(X[count_sum[irep]:count_sum[irep+1]-1,.])';
    threadendfor;
        
    retp(Wn_X);
endp;

proc 1 = mundlak(X,count);
    local count_sum, X_bar;
    
    count_sum   = cumsumc(1|count);
    X_bar       = zeros(rows(X),cols(X));
    
    threadfor irep(1,rows(count_sum)-1,1);
        X_bar[count_sum[irep]:count_sum[irep+1]-1,.]	= zeros(rows(X[count_sum[irep]:count_sum[irep+1]-1,.]),cols(X)) + meanc(X[count_sum[irep]:count_sum[irep+1]-1,.])';     
    threadendfor;
    
    retp(X_bar);
endp;

proc 1 = mundlak_se(X,count);
    local count_sum, se_X_bar;
    
    count_sum   = cumsumc(1|count);
    se_X_bar    = zeros(rows(X),cols(X));
    
    threadfor irep(1,rows(count_sum)-1,1);
        se_X_bar[count_sum[irep]:count_sum[irep+1]-1,.]	= zeros(rows(X[count_sum[irep]:count_sum[irep+1]-1,.]),cols(X)) + stdc(X[count_sum[irep]:count_sum[irep+1]-1,.])';     
    threadendfor;
    
    retp(se_X_bar);
endp;


proc 1 = _1Way_effects(y,X,count,b_Wn);
local a_i, count_sum;

    a_i         = zeros(rows(y),1);
    count_sum   = cumsumc(1|count);
    
    threadfor irep(1,rows(count_sum)-1,1);       
        a_i[count_sum[irep]:count_sum[irep+1]-1,.]	= zeros(rows(y[count_sum[irep]:count_sum[irep+1]-1,.]),1) 
                                                            + meanc(y[count_sum[irep]:count_sum[irep+1]-1,.]) - (meanc(X[count_sum[irep]:count_sum[irep+1]-1,.])' *b_Wn);     
    threadendfor;

retp(a_i);
endp;

proc 2 = _effects_hat(y,X,b_Within,Q_inv_D,THETA_1,THETA_2,THETA_3);
    local resids, alpha_hat,lambda_hat;
    
    resids      = Y - (X*b_Within);
    lambda_hat  = Q_inv_D*resids; 
    alpha_hat   = (THETA_1+THETA_2-THETA_3)*resids;

retp(alpha_hat,lambda_hat);
endp;


proc 3 = _i_and_t_dummies(year,id);
	local Delta_1, Delta_2, DELTA_N_inv, i_sel, D_N, D_i, t_rep;
    
    Delta_1 = {};
    Delta_2 = {};

    for t_rep (1,maxc(year),1);

        "t_rep~T";;t_rep~maxc(year);

        i_sel   = selif(id, year .eq t_rep);

        D_N     = eye(maxc(id));
        D_i     = D_N[i_sel,.];
    
        Delta_1 = Delta_1|D_i;

    endfor;

    DELTA_N_inv     = invpd(Delta_1'Delta_1);

    Delta_2 = dummy(year, seqa(1,1,maxc(year)-1));

retp(Delta_1,Delta_2,DELTA_N_inv);
endp;

proc 6 = _2way(X,y,Delta_1,Delta_2,DELTA_N_inv);
	local A, D_tild, Q, P, WnY, WnX, THETA_1, THETA_2, THETA_3, Q_inv_D, Q_inv;
        
    A           = Delta_2' * Delta_1;
    D_tild      = Delta_2 - (Delta_1*DELTA_N_inv*(A'));
    Q           = (Delta_2' * Delta_2) - (A*DELTA_N_inv*A');
    Q_inv       = pinv(Q);
    //P           = eye(rows(Delta_1)) - (Delta_1*DELTA_N_inv*(Delta_1')) - (D_tild*Q_inv*(D_tild')); 
    P           = eye(rows(Delta_1)) - (Delta_1*DELTA_N_inv*Delta_1') - (D_tild*Q_inv*D_tild'); 
    WnY         = P*Y;
    WnX         = P*X;

    clear P, Y, X;

    THETA_1         = DELTA_N_inv*Delta_1';
    THETA_2         = DELTA_N_inv*(A')*Q_inv*Delta_2';
    THETA_3         = DELTA_N_inv*(A')*Q_inv*A*DELTA_N_inv*Delta_1';

    clear A, DELTA_N_inv;

    Q_inv_D         = Q_inv*D_tild';

    clear A, D_tild, Q, P, DELTA_N_inv;

retp(WnY,WnX,Q_inv_D,THETA_1,THETA_2,THETA_3);
endp;

proc 5 = _ols(y,X,df_correction); 	
    local b_OLS, resid_OLS, df_OLS, sigma2_OLS, sigma_OLS, stderr_OLS, b_OLS_all, SSE, logL;

    trap 1;
    b_OLS       = y/X; 
    if scalerr(b_OLS);
        b_OLS = inv(X'X)*X'y;
    endif;
    trap 0;
    resid_OLS   = y - (X*b_OLS);
    SSE         = _sse_calc(y,X*b_OLS);
    df_OLS      = rows(b_OLS)+df_correction;
    sigma2_OLS  = sse / (rows(resid_OLS) - df_OLS);
    sigma_OLS   = sqrt(sigma2_OLS);
    trap 1;
    stderr_OLS  = sqrt(diag(sigma2_OLS .* invpd(moment(X,0))));
    if scalerr(stderr_OLS);
        stderr_OLS = sqrt(abs(diag(sigma2_OLS .* inv(X'X))));
    endif;
    trap 0;
    b_OLS_all   = b_OLS~stderr_OLS~abs(b_OLS ./ stderr_OLS);
    
    //logL	    = ln((1/sqrt(sse/(rows(resid_OLS)))) .* pdfn(resid_OLS ./ sqrt(sse/(rows(resid_OLS)))));	
    logL	    = -(rows(y)/2)*(1 + ln(2*pi) + ln(SSE/rows(y)));
    
retp(b_OLS_all, sigma2_OLS, sse, logL, sumc(logL));
endp;


proc 1 = _IC(logL,N,k);
local BIC, AIC, CAIC, HQIC;	
	
	BIC   = (-2*logL) + (ln(N)*k);  
	AIC   = (-2*logL) + (2*k);  
	CAIC  = (-2*logL) + ((1+ln(N))*k);  
	HQIC  = (-2*logL) + ((2*ln(ln(N)))*k);  

retp(BIC~AIC~CAIC~HQIC);
endp;

proc 1 = _sse_calc(y,y_hat);
local e;
  e = y - y_hat;
retp(e'e);
endp;
