new;
cls;
trap 1;
library pgraph;
t_Start = date();

#include F:\mark\gauss\procs\Within_trans_threaded.prc;

/*
** Perform import
*/
fname_orig = "F:/Mark/data/HILDA21/HILDA_BMI_W21.dat";
BMI_data    = loadd(fname_orig);
headers     = getHeaders(fname_orig);
call dstatmt(fname_orig);

/* set an indicator for males/female */
gender      = 1;        //hgsex = 1 are males; = 2 are females
BMI_data    = selif(BMI_data,(BMI_data[.,"HGSEX"] .eq gender));

//xwaveid     -  wave id
//id           - id coded
//wave        - wave/time
//ghgh        - sf-36 general health
//ghmh       - sf-36 mental health
//bmi          - bmi levels
//bmi_cat1    - bmi categories 1
//bmi_cat2   - bmi categories 2
//hgsex      - gender
//hgage      - age
//esbrd      - Current labour force status
//edhigh     - highest education
//mrcurr      -  Marital status
//lssmkf [lssmkf_n] - smoking [smoking modified]


/* DEFINE DATA */

year	= BMI_data[.,"WAVE"] - minc(BMI_data[.,"WAVE"]) + 1;
id      = BMI_data[.,"ID"];
t_dumm  = dummy(year,seqa(minc(year),1,maxc(year)-1));
y		= BMI_data[.,"GHGH"];		                                           
y_name  = "GHGH"; 
                                            
X		= ln(BMI_data[.,"HGAGE"])~
            (BMI_data[.,"EDHIGH1"] .le 3)~
            BMI_data[.,"EQ_DISP_"]~
            (BMI_data[.,"LSSMKF_N"] .eq 2)~(BMI_data[.,"LSDRKF_N"] .eq 2)~(BMI_data[.,"PHYS_2"] .ge 2)~(BMI_data[.,"HELTH_N"] .eq 1)~   
            (BMI_data[.,"MARR_DE"] .eq 1)~(BMI_data[.,"ESBRD"] .eq 1)~ //(BMI_data[.,"HECRPA_N"] .ge 1)~ 
            t_dumm[.,2:maxc(year)];

missings    = missrv(BMI_data[.,"HGAGE"
                        "EDHIGH1" 
                        "EQ_DISP_" 
                        "LSSMKF_N" "LSDRKF_N" "PHYS_2" "HELTH_N" 
                        "MARR_DE" "ESBRD"] ~ 
                        t_dumm[.,2:maxc(year)],-999);
                        
X           = miss((X .* (missings .ne -999)) + (-999 .* (missings .eq -999)),-999);
                 
X_names	= "ln_age" $| "hi_ed" $|  
            "eq_disp_inc" $|
            "smoker" $| "drinker" $| "active" $| "lt_health" $|
            "married" $| "employed" $|
            "wave 7" $| "wave 8" $| "wave 9" $| "wave 10" $| "wave 11" $| "wave 12"
            $| "wave 13" $| "wave 14" $| "wave 15" $| "wave 16" $| "wave 17" $| "wave 18"
            $| "wave 19" $| "wave 20" $| "wave 21";

k_X			        = cols(X)-1;
spline_cut_points   = 10~60;
Wn_cut_points       = 20;
x_spline            = BMI_data[.,"BMI"];
spline_name 	    = "BMI";
id_name	            = "id";
t_var               = "year";


/* call the dataprep function */

fname_out   = "BMI_FE_estimation_data.dat";
{count}     = _panel_data_prep_spline(fname_out,id,year,y,X,x_spline,y_name,X_names,spline_name,spline_cut_points,Wn_cut_points);
call dstatmt(fname_out);

/* LOAD IN PREPARED DATASET AND DEFINE VARIABLES */

BMI_est_data    = loadd(fname_out);
headers         = getHeaders(fname_out);
BMI_est_data    = sortmc(BMI_est_data,1|2);
y		        = BMI_est_data[.,y_name];
X               = BMI_est_data[.,X_names];
BMI             = BMI_est_data[.,spline_name];
NT              = rows(BMI_est_data);
N               = rows(count);

{WX}	= Within(X,count);
{Wy}	= Within(y,count);

k_x     = cols(WX);

{Wn_BMI}        = Within(BMI,count);
WX_all_FIXED    = WX~Wn_BMI;

df_correction = N;
{b_Wn_FIXED, sig2_FIXED, sse_FIXED, logL_FIXED, sumLogL_FIXED}  = _ols(Wy,Wx_all_FIXED,df_correction); 	

ICs_fixed  	    = _IC(sumLogL_fixed,NT,rows(b_Wn_fixed)+df_correction);  
BIC_fixed	    = ICs_fixed[1];

/* TIE THE SPLINES? */

tie = 0;

/* SEARCH FOR OPTIMAL 1-BREAK MODEL: THREADED */

/* RANGE OF SEARCH VALUES AND CONTROL VARIABLES */

WHO_bands_search       = 18.5|25|30|40;

//percentiles or 0.5 increment?
fix_or_percent  = "fix"; //"percent" for percentiles
if fix_or_percent=="percent";
    min_pc          = 0.01;
    e               = seqa(min_pc, min_pc, (1/min_pc)-1);
    search_vec      = quantile(BMI, e);
    range           = minc(search_vec)~maxc(search_vec);
elseif fix_or_percent=="fix";
    range           = 16~55;
    min_pc          = 0.5; //increment here
    n_steps         = floor((range[2] - range[1]) / min_pc) + 1;
    search_vec      = seqa(range[1],min_pc,n_steps);
endif;    

search_vec      = unique(sortc(search_vec|WHO_bands_search,1));
nthreads        = rows(search_vec);
sumLogL_mat     = zeros(nthreads,3);

?;
"OPTIMAL SINGLE 1-BREAK";?;
threadfor i (1,nthreads,1);
    
    {b_Wn_1BP, sig2_1BP, sse_1BP, logL_FE_1, sumLogL_mat[i,1]}  = 
            _ols(Wy,WX~Within(_splinex(BMI,search_vec[i,.],tie),count),df_correction);
    	
    sumLogL_mat[i,2]  = search_vec[i,.];
    sumLogL_mat[i,3]  = k_x+cols(_splinex(BMI,search_vec[i,.],tie))+df_correction;
    
threadEndFor;

ICs_1BP = zeros(nthreads,4);
threadfor i (1,nthreads,1);

    ICs_1BP[i,.]  = _IC(sumLogL_mat[i,1],NT,sumLogL_mat[i,3]);  	
    
threadEndFor;
ICs_1BP     = sortc(ICs_1BP[.,1]~sumLogL_mat[.,2:3],1);
BIC_opt1    = ICs_1BP[1,.];
tau_1BP     = ICs_1BP[1,2];


/* SET K, MAX NUMBER OF BREAKPOINTS TO SEARCH FOR */

K           = 3;
search_mat  = _search_mat(K,search_vec);
nthreads    = rows(search_mat);


/* OPTIMAL K BREAK MODEL */ 

sumLogL_mat     = zeros(nthreads,K+2);

?;
"OPTIMAL MODEL";?;
threadfor i (1,nthreads,1);

    {b_Wn_OPT, sig2_OPT, sse_OPT, logL_FE_OPT, sumLogL_mat[i,1]}  = 
    
            _ols(Wy,WX~Within(_splinex(BMI,search_mat[i,.],tie),count),df_correction);
    
    sumLogL_mat[i,2:K+1]  = search_mat[i,.];
    sumLogL_mat[i,K+2]    = k_x+cols(_splinex(BMI,search_mat[i,.],tie))+df_correction;

    "rep, out of:";;i~nthreads;    

threadEndFor;

ICs_OPT = zeros(nthreads,4);

threadfor i (1,nthreads,1);
    ICs_OPT[i,.]  = _IC(sumLogL_mat[i,1],NT,sumLogL_mat[i,K+2]);  	
threadEndFor;

ICs_OPT         = ICs_OPT[.,1]~sumLogL_mat[.,2:K+1];
ICs_OPT         = sortc(ICs_OPT,1);
BIC_OPT         = ICs_OPT[1,1];
breaks_OPT      = unique(ICs_OPT[1,2:K+1]);
k_splines_OPT   = rows(breaks_OPT);

/* re-estimate with optimal 1-break model */

{x_spline}  = _splinex(BMI,tau_1BP,tie);
{Wx_spline} = Within(x_spline,count);
WX_all_1BP	= WX~Wx_spline;

{b_Wn_1BP, sig2_1BP, sse_1BP, logL_FE_1, sumLogL_FE_1}  = _ols(Wy,WX_all_1BP,df_correction);

/* re-estimate with optimal model */

{x_spline}  = _splinex(BMI,breaks_OPT',tie);
{Wx_spline} = Within(x_spline,count);
WX_all_OPT	= WX~Wx_spline;

{b_Wn_OPT, sig2_OPT, sse_OPT, logL_FE_OPT, sumLogL_FE_OPT}  = _ols(Wy,WX_all_OPT,df_correction);

fixed_vars  = X_names $|spline_name;
vars_1BP    = X_names $|"BMI_low" $|"BMI_high";
vars_2BP    = X_names $|"BMI_low" $|"BMI_med" $|"BMI_high";
vars_3BP    = X_names $|"BMI_1" $|"BMI_2" $|"BMI_3" $|"BMI_4";
vars_4BP    = vars_3BP $|"BMI_5";    
vars_5BP    = vars_4BP $|"BMI_6";    

if k_splines_OPT eq 1;
    vars_OPT = vars_1BP;
elseif k_splines_OPT eq 2;
    vars_OPT = vars_2BP;
elseif k_splines_OPT eq 3;
    vars_OPT = vars_3BP;
elseif k_splines_OPT eq 4;
    vars_OPT = vars_4BP;
endif;


/* CONSTRUCT A LINEAR PREDICTION ON ALL MODELS */

data_sort	    = sortc(BMI~y~X,1);
BMI_sort        = data_sort[.,1];
y_sort          = data_sort[.,2];
X_sort          = data_sort[.,3:cols(data_sort)];
BMI_X_axis      = seqa(10,0.1,501);

X_spline_sort   = BMI_X_axis;
X_spline        = BMI;
X_fixed         = X~X_spline;
b_spline        = b_Wn_FIXED[k_X+1:rows(b_Wn_FIXED[.,1]),1];
{a_i}           = _1Way_effects(y,X_fixed,count,b_Wn_FIXED[.,1]);
a_i_sort        = sortc(BMI~a_i,1);
a_i             = a_i_sort[.,2];
y_hat_0         = meanc(a_i + X_sort[.,1:k_X]*b_Wn_FIXED[1:k_X,1]) + (BMI_X_axis*b_spline);

X_spline_sort   = _splinex(BMI_X_axis,tau_1BP,tie);
X_spline        = _splinex(BMI,tau_1BP,tie);
X_1BP           = X~X_spline;
b_spline        = b_Wn_1BP[k_X+1:rows(b_Wn_1BP[.,1]),1];
{a_i}           = _1Way_effects(y,X_1BP,count,b_Wn_1BP[.,1]);
a_i_sort        = sortc(BMI~a_i,1);
a_i             = a_i_sort[.,2];
y_hat_1         = meanc(a_i + X_sort[.,1:k_X]*b_Wn_1BP[1:k_X,1]) + (X_spline_sort*b_spline);

X_spline_sort   = _splinex(BMI_X_axis,breaks_OPT',tie);
X_spline        = _splinex(BMI,breaks_OPT',tie);
X_OPT           = X~X_spline;
b_spline        = b_Wn_OPT[k_X+1:rows(b_Wn_OPT[.,1]),1];
{a_i}           = _1Way_effects(y,X_OPT,count,b_Wn_OPT[.,1]);
a_i_sort        = sortc(BMI~a_i,1);
a_i             = a_i_sort[.,2];
y_hat_OPT       = meanc(a_i + X_sort[.,1:k_X]*b_Wn_OPT[1:k_X,1]) + (X_spline_sort*b_spline);

/*
e_0        = zeros(rows(BMI_sort),1);
M               = sig2_3BP .* invpd(moment(X_sort~X_spline_sort,0));
threadFor i (1,rows(BMI_sort),1);
    e_0[i]        = sig2_3BP + ((zeros(1,k_X)~X_spline_sort[i,.])*M*(zeros(1,k_X)~X_spline_sort[i,.])');
threadEndFor;
*/
    
//PI_upper = y_hat + (1.96 .* sqrt(e_0));
//x = cdfTci(0.025, rows(BMI_sort)-df_correction-rows(b_Wn_3BP));
//x;    
    
/* WHO BANDS */

WHO_bands       = 18.5~25~30;
X_spline_sort   = _splinex(BMI_X_axis,WHO_bands,tie);
X_spline        = _splinex(BMI,WHO_bands,tie);
X_WHO           = X~X_spline;

{Wx_spline}     = Within(X_spline,count);
WX_all_WHO	    = WX~Wx_spline;	

{b_Wn_WHO, sig2_WHO, sse_WHO, logL_WHO, sumLogL_WHO}  = _ols(Wy,WX_all_WHO,df_correction);

ICs_WHO         = _IC(sumLogL_WHO,NT,rows(b_Wn_WHO)+df_correction);  
BIC_WHO	        = ICs_WHO[1]~WHO_bands;

b_spline        = b_Wn_WHO[k_X+1:rows(b_Wn_WHO[.,1]),1];
{a_i}           = _1Way_effects(y,X_WHO,count,b_Wn_WHO[.,1]);
a_i_sort        = sortc(BMI~a_i,1);
a_i             = a_i_sort[.,2];
y_hat_WHO       = meanc( a_i + X_sort[.,1:k_X]*b_Wn_WHO[1:k_X,1,1]) + (X_spline_sort*b_spline);


/* WHO DUMMIES */

d_18    = BMI .lt 18.5;
d_18_25 = (BMI .ge 18.5) .and (BMI .lt 25);
d_25_30 = (BMI .ge 25) .and (BMI .lt 30);
d_gt_30 = (BMI .ge 30);

d_18_sort       = BMI_X_axis .lt 18.5;
d_18_25_sort    = (BMI_X_axis .ge 18.5) .and (BMI_X_axis .lt 25);
d_25_30_sort    = (BMI_X_axis .ge 25) .and (BMI_X_axis .lt 30);
d_gt_30_sort    = (BMI_X_axis .ge 30);
d_sort          = d_18_sort~d_18_25_sort~d_25_30_sort~d_gt_30_sort;

X_dummies       = X~d_18~d_18_25~d_25_30~d_gt_30;
{WX_dummies}    = Within(X_dummies,count);

//b_Wn_dummies    = inv(Wx_dummies'Wx_dummies)*Wx_dummies'wy;
{b_Wn_dummies, sig2_dummies, sse_dummies, logL_dummies, sumLogL_dummies}  = _ols(Wy,WX_dummies,df_correction);

ICs_dummies     = _IC(sumLogL_dummies,NT,rows(b_Wn_dummies)+df_correction);  
BIC_dummies	    = ICs_dummies[1]~WHO_bands;

b_spline        = b_Wn_dummies[k_X+1:rows(b_Wn_dummies[.,1]),1];
{a_i}           = _1Way_effects(y,X_dummies,count,b_Wn_dummies[.,1]);
a_i_sort        = sortc(BMI~a_i,1);
a_i             = a_i_sort[.,2];
y_hat_dummies   = meanc(a_i + X_sort[.,1:k_X]*b_Wn_dummies[1:k_X,1,1]) + (d_sort*b_spline);

/* COMBINE IC'S */

BICs_opt    = (BIC_dummies[1]~-99)|(BIC_WHO[1]~-9)|(BIC_fixed~0)|(BIC_opt1[1]~11)|(BIC_OPT[1]~k_splines_OPT);
BICs_opt    = sortc(BICs_opt,1);

/*
save LogL_FE_fixed, df_FE_fixed, 
    LogL_FE_1, df_FE_1,
    LogL_FE_2, df_FE_2,
    LogL_FE_3, df_FE_3,
    LogL_FE_4, df_FE_4,
    LogL_FE_5, df_FE_5,
    LogL_FE_6, df_FE_6
    ;
*/

t_End   = date();
runtime = etstr(ethsec(t_Start,t_End)); 


           /* CHANGE DIRECTORY/FILENAME AS APPROPRIATE BELOW */

//NOTE: file will automatically save to the "y" name, whether tied or not and the % search value  
file_name ="F:\\Mark\\Gauss\\output\\discrete_threshold\\BMI_simultaneous_BPs_percentile_FE_big_spec_";

pcent_string=ftostrC(min_pc,"%2.2f");
K_string=ftostrC(K,"%1.0f");
if tie;
    tie_string = "_tied_K";
else;
    tie_string = "_untied_K";
endif;

if gender eq 1;
    gender_string = "_males";
else;
    gender_string = "_females";
endif;

file_name_pc = file_name $+ y_name $+ "_" $+ pcent_string $+ tie_string $+ K_string $+ gender_string $+ ".out";
file_name_pc = strcombine(file_name_pc, "", 0);

output file = ^file_name_pc reset;
output on;

format 15,6;

?;
"               OPTIMAL BMI MODEL SIMULTANEOUS APPROACH: ALL BREAKPOINTS ";
?;
"FILE IS BMI_simultaneous_2Way_percentiles_2023_gender_big_spec.e";
?;
"DATE IS";;datestring(0);
?;
"DSTATS";
dstatmt(fname_out);

?;
sprintf("%-40s  %-40s", "Gender sample is:", gender_string);
sprintf("%-40s  %-4.1f %-4.1f", "range of threshold variable", minc(BMI), maxc(BMI));
sprintf("%-40s  %-40s", "percentiles or fixed increments", fix_or_percent);
sprintf("%-40s  %-3.1f %-3.1f", "search range:", range[1], range[2]);
sprintf("%-40s  %-3.3f", "minimum %/increment in spline variables", min_pc);
sprintf("%-40s  %-40s", "approach", "simultaneous");
sprintf("%-40s  %-1.0f", "max. no. of splines searched for:", K);
sprintf("%-40s  %-1.0f", "no. of estimated models:", rows(search_mat));
sprintf("%-40s  %-40s", "run-time (days,hours,minutes)", runtime);?;

"*** IC'S ***";

?;"RANKED MODEL BY BIC, WITH NUMBER OF BREAKS";?;
sprintf("%12s %12s %12s %12s","-99=DUMMIES;"," -9=WHO;"," 11=OPTIMAL 1BP;"," #=>0 OPTIMAL;"  );
sprintf("%10s  %7s","   BIC", "  # breaks");
sprintf("%12.1f  %7.0f", BICs_opt[.,1], BICs_opt[.,2]);

?;"OPTIMAL 1 BREAKPOINT MODEL: IC VALUE; BREAKPOINT";
sprintf("%10s  %7s","   BIC", "  breaks");
sprintf("%12.1f  %7.1f", BIC_opt1[.,1], BIC_opt1[.,2]);

?;"***OPTIMAL*** BREAKPOINT MODEL: IC VALUE; BREAKPOINT";
sprintf("%10s  %7s","   BIC", "  breaks");
sprintf("%10.1f  %7.1f", BIC_OPT[1,1], breaks_OPT');


/* ESTIMATED MODELS */
?;
"*** ESTIMATED MODELS ***";

?;
"FIXED COEFF MODEL";?;
sprintf("%12s  %7s  (%4s)  %4s", "", "Coeff", "s.e.", "t-");
sprintf("%12s  %7.4f  (%5.3f)  %5.2f", fixed_vars, b_Wn_FIXED[.,1], b_Wn_FIXED[.,2], b_Wn_FIXED[.,3]);

?;
"USUAL WHO FIXED BANDS";?;
"Break point occurs at";;WHO_bands;?;
sprintf("%12s  %7s  (%5s)  %4s", "", "Coeff", "s.e.", "Z-");
if maxc(vec(WHO_bands)) eq 40;
    sprintf("%12s  %7.4f  (%5.3f)  %4.2f", vars_4BP, b_Wn_WHO[.,1], b_Wn_WHO[.,2], b_Wn_WHO[.,3]);
else;
    sprintf("%12s  %7.4f  (%5.3f)  %4.2f", vars_3BP, b_Wn_WHO[.,1], b_Wn_WHO[.,2], b_Wn_WHO[.,3]);
endif;
?;

"USUAL WHO FIXED BANDS (DUMMIES)";?;
"Break point occurs at";;WHO_bands;?;
sprintf("%12s  %7s  (%5s)  %4s", "", "Coeff", "s.e.", "Z-");
sprintf("%12s  %7.4f  (%5.3f)  %4.2f", vars_3BP, b_Wn_dummies[.,1], b_Wn_dummies[.,2], b_Wn_dummies[.,3]);

?;
"***OPTIMAL*** BREAKPOINT MODEL";?;
"Break point occurs at";;breaks_OPT';?;
sprintf("%12s  %7s  (%5s)  %4s", "", "Coeff", "s.e.", "Z-");
sprintf("%12s  %7.4f  (%5.3f)  %4.2f", vars_OPT, b_Wn_OPT[.,1], b_Wn_OPT[.,2], b_Wn_OPT[.,3]);

/* PLOT FITTED VALUES */

plotHistP(BMI, 30);

//Declare the structure 
struct plotControl myPlot;

//Initialize the structure 
myPlot = plotGetDefaults("xy");

plotSetGrid(&myPlot, "on");
plotSetYTicCount(&myPlot, 5);
thickness = 1~1~3~1~1;
plotSetLineThickness(&myPlot, thickness);
clrs = "aqua"$|"midnight blue"$|"red"$|"orange"$|"brown";
plotSetLineColor(&myPlot, clrs);

plotSetXLabel(&myPlot, "BMI", "verdana", 10, "black");
plotSetYLabel(&myPlot, y_name, "verdana", 10, "black");
my_title = "BMI and Health Relationship: " $+ y_name;
//plotSetTitle(&myPlot, "BMI and Health Relationship", "verdana", 12);
plotSetTitle(&myPlot, my_title, "verdana", 12);
//plotSetTitle(&myPlot, yvars);
label = "Fixed"$|"1 Break"$|"OPT Breaks"$|"WHO"$|"WHO dummies";
location = "bottom left";
orientation = 0;
plotSetLegend(&myPlot, label, location, orientation);

plotxy(myPlot, BMI_X_axis, y_hat_0~y_hat_1~y_hat_OPT~y_hat_WHO~y_hat_dummies);

if gender eq 1;     //males
    
    if tie;
        
        BMI_fixed_males = y_hat_0;
        BMI_opt_males   = y_hat_OPT;
        BMI_WHO_males   = y_hat_WHO;
        save BMI_X_axis, BMI_fixed_males, BMI_opt_males, BMI_WHO_males;    
        
    else;
        
        BMI_opt_males_untied   = y_hat_OPT;
        BMI_WHO_males_untied   = y_hat_WHO;
        save BMI_X_axis, BMI_fixed_males, BMI_opt_males_untied, BMI_WHO_males_untied;
        
    endif;
    
else;               //females
    
    if tie;
    
        BMI_fixed_females   = y_hat_0;
        BMI_opt_females     = y_hat_OPT;
        BMI_WHO_females     = y_hat_WHO;
        save BMI_X_axis, BMI_fixed_females, BMI_opt_females, BMI_WHO_females;
        
    else;
        
        BMI_opt_females_untied   = y_hat_OPT;
        BMI_WHO_females_untied   = y_hat_WHO;
        save BMI_X_axis, BMI_fixed_females, BMI_opt_females_untied, BMI_WHO_females_untied;
        
    endif;
        
endif;



/* re-do for a subset of key results */

//Initialize the structure 
graphset;
myPlot = plotGetDefaults("xy");

plotSetGrid(&myPlot, "on");
plotSetYTicCount(&myPlot, 5);
thickness = 1~1~1;
plotSetLineThickness(&myPlot, thickness);
clrs = "black";
plotSetLineColor(&myPlot, clrs);

plotSetXLabel(&myPlot, "BMI", "verdana", 16, "black");
plotSetYLabel(&myPlot, y_name, "verdana", 16, "black");
//my_title = "BMI and Health Relationship: " $+ yvars;
//plotSetTitle(&myPlot, my_title, "verdana", 12);
//plotSetTitle(&myPlot, yvars);
label = "Optimal"$|"WHO Ranges";
location = "bottom left";
orientation = 0;
plotSetLegend(&myPlot, label, location, orientation);

// Set font of legend
plotSetLegendFont(&myPlot, "times", 16, "black");


//newSymbol = { 0, -1, 2 };
//symbolWidth = 5;
//plotSetLineSymbol(&myPlot, newSymbol, symbolWidth);

newStyle = { 2, 1};
plotSetLineStyle(&myPlot, newStyle);

plotxy(myPlot, BMI_X_axis, y_hat_OPT~y_hat_WHO);

ret = xlsWriteM(BMI_X_axis~y_hat_OPT~y_hat_WHO, "BMI_fit.xlsx");



output off;
closeall;

stop;


proc 1 = _splinex(x,gama,tie);
local xspline, dummies, gam_mat, x_minus;
    
    if rows(gama) eq 1;
        gama = gama';
    endif;

	gama = unique(gama,1);
    gama = sortc(gama,1);

    if tie;
        gam_mat = gama' .* ones(rows(x),1);
        x_minus = (x .* ones(1,cols(gam_mat))) - gam_mat;
        dummies = x .ge gam_mat;
        xspline = x~(x_minus .* dummies);
    else;
        xspline = dummy(x, gama)  .* x;
    endif;
    
retp(xspline);
endp;

