flush(stdout)  # Flush any buffered output before proceeding
# change all directories to current one
script_dir = dirname(@__FILE__)
cd(script_dir)
println(script_dir)

#Import the functions required. 
include("FE_thsearch_functions.jl")
# Print number of threads
println("Number of threads")
println(Threads.nthreads())
println()

#Import the data. 
df = DataFrame(load("HILDA_BMI_W21.dta"))
#label output file; note number of max breaks, K, will automatically be added to this name
output_file = "BMI_large_spec_"

#Sort by id and then wave
#name the id, wave/year, spline and dependent variable
id_name         = Symbol("id")
wave_name       = Symbol("wave")
spline_name     = Symbol("bmi")
y_name          = Symbol("ghgh")
df_FE_big_spec  = sort(df, [id_name, wave_name])

println("DSTATS: original file")
println(describe(df_FE_big_spec))
println()
println("Size of dataframe")
println(size(df_FE_big_spec))

#error("Script execution stopped.")

#Define any additional X's here
#Use this to create any dummy variables from categorical ones 
print_it                = "yes"    #"yes" or "no" to print cycling through the dummies; :wave (wave_name) is factor variable name; "text" is variable prefix 
df_FE_big_spec, wave_dummy_names = dummy_vars(df_FE_big_spec, wave_name,"wave",print_it)

gender      = 2;        #hgsex = 1 are males; = 2 are females
#select_var  = Bool.(df_FE_big_spec.hgsex .== gender)
select_var = df_FE_big_spec.hgsex .== gender
#set a switch to let procedure you'll be sample selecting or not, yes/no and name to be added to output file
select_sample = "no"

if select_sample == "yes"
    if gender ==2
        split_sample_name = "female_"
        df_FE_big_spec.select_var = select_var
    else
        split_sample_name = "male_"
        df_FE_big_spec.select_var = select_var
    end
    output_file         = output_file * split_sample_name

else
    output_file         = output_file * "full_sample_"
end


#Trim extreme values of BMI; note these are then removed from the dataset
cut_offs = 10,60    #set to -Inf,Inf if not required
println()
println("BMI cut-offs")
print(cut_offs)
println()
Wn_maximums = Dict(:Wn_bmi => 20, :Wn_ghgh => 50)
Wn_maximums = Dict(:Wn_bmi => 20)

#Include additional variables here
#select/define additional variables here that aren't already defined in dataframe, and add to dataframe
df_FE_big_spec.ln_age       = log.(df_FE_big_spec[:,:hgage])
df_FE_big_spec.hi_ed        = df_FE_big_spec[:,:edhigh1] .<= 3
df_FE_big_spec.smoker       = df_FE_big_spec[:,:lssmkf_n2] .== 2
df_FE_big_spec.drinker      = df_FE_big_spec[:,:lsdrkf_n2] .== 2
df_FE_big_spec.active       = df_FE_big_spec[:,:phys_2] .>= 2
df_FE_big_spec.lt_health    = df_FE_big_spec[:,:helth_n] .== 1
df_FE_big_spec.married      = df_FE_big_spec[:,:marr_de] .== 1
df_FE_big_spec.employed     = df_FE_big_spec[:,:esbrd] .== 1
df_FE_big_spec.disabled     = df_FE_big_spec[:,:hecrpa_n] .>= 1
X_additional                = [Symbol(name) for name in ["ln_age", "hi_ed", "smoker", "drinker", "active", "lt_health", "married", "employed", "disabled"]]
X_additional                = [Symbol(name) for name in ["ln_age", "hi_ed", "smoker", "drinker", "active", "married", "employed", "disabled"]]
X_additional                = [Symbol(name) for name in ["ln_age", "hi_ed", "smoker", "drinker", "active", "lt_health", "married", "employed"]]
X_additional                = [Symbol(name) for name in ["ln_age", "hi_ed", "eq_disp_inc_n", "smoker", "drinker", "active", "lt_health", "married", "employed"]]
#X_additional                = [Symbol(name) for name in ["ln_age"]]

#X_name should NOT include the spline variable, which is entered separately
X_name  = vcat(X_additional,wave_dummy_names[2:end])

#=
Data prep function: creates Within and mean variables; trims dataset if required (on the basis of the spline_name variable), removes singleton obs, 
drops missings and selects on the basis of the select_var .==1: ommit if no sample selection
=#
df_FE_big_spec, cum_sum, N   = data_prep(df_FE_big_spec,id_name,spline_name,y_name,X_name,cut_offs,Wn_maximums)

# Perform descriptive statistics of data frame
# Enter "yes" to include Withins and means in DSTATS tables, "no" to exclude
include_Wns_DSTATS = "yes"  
DSTATS = print_df_DSTATS(df_FE_big_spec,include_Wns_DSTATS)
println("DSTATS: estimation dataframe")
println(DSTATS)
println()
println("Size of dataframe")
println(size(df_FE_big_spec))
if select_sample == "yes"
    println("Sample has been selected by:")
    println(split_sample_name)
end

#SET-UP MODEL RUNNING PARAMETERS
#set K=no. of potential breaks
K=4

#include a constant term in FE regressions? no is recommended
constant = "no"

#print search iterations?
print_search = "no"

#define search range parameters
WHO_search_breaks   = vcat(18.5, 25, 30, 40)    #enter additional values to include in search; if none enter [-999] or delete from function call
#WHO_search_breaks = [-999]
#WHO_search          = vcat(18.5, 25, 30)        #enter the WHO breaks for comparison models; doesn't affect the search values
search_range = vcat(16, 55)                 #enter range for search procedure
#search_range = vcat(11, 59)                 #enter range for search procedure
spline = [0]                                #enter the spline variable for percentiles; and [0] if fixed increments 
step = 0.5                                  #enter the incremental values for search when spline = [0] or percentiles, e.g., 0.1 otherwise

search_vec, n_threads = search_values(search_range, step, spline, WHO_search_breaks)
#search_vec, n_threads = search_values(search_range,min_pc,spline)


# EVERYTHING BELOW IS AUTOMATED; RESULTS WILL BE PRINTED TO SCREEN AND TO SPECIFIED OUTPUT FILE
# ---------------------------------------------------------------------------------------------- #
Wny_name, WnX_fixed = Wn_names(y_name, X_name, spline_name)
df_correction = N
b_Wn, σ², SSE, logL, BIC_fixed = ols(Wny_name, WnX_fixed, constant, df_correction, df_FE_big_spec)

BIC_fixed = hcat(BIC_fixed, 0)

if constant == "yes"
    push!(X_name, :one)
end

#data is a matrix which must be in the order: id, threshold variable (not Within transformed!), Wn_y, Wn_X1,... 
data = Matrix(df_FE_big_spec[:, [id_name, spline_name, Wny_name, WnX_fixed[1:end-1]...]])

search_mat = define_search_mat(search_vec,K)
println()
@printf("%-35s %-4d\n", "number of search models: ",size(search_mat,1))
@printf("%-35s %-6s\n", "Spline variable:",spline_name)
@printf("%-35s %-4.1f %-4.1f\n", "Dataframe trunction on above by: ",cut_offs[1],cut_offs[2])
@printf("%-35s %-4.1f %-4.1f\n", "Search range on above variable: ",search_range[1],search_range[2])
keys_array = collect(keys(Wn_maximums))
values_array = collect(values(Wn_maximums))
if length(keys_array) == 1
    first_key = first(keys_array)
    first_value = first(values_array)
    @printf("%-35s %-6s, %-4.1f\n", "Wn_maximums", first_key, first_value)
elseif length(keys_array) == 2
    second_key = keys_array[2]
    second_value = values_array[2]
    @printf("%-35s %-6s, %-4.1f, %-6s, %-4.1f\n", "Wn_maximums", keys_array[1], values_array[1], second_key, second_value)
elseif length(keys_array) == 3
    second_key = keys_array[2]
    second_value = values_array[2]
    third_key = keys_array[3]
    third_value = values_array[3]
    @printf("%-35s %-6s, %-4.1f, %-6s, %-4.1f, %-6s, %-4.1f\n", "Wn_maximums", keys_array[1], values_array[1], second_key, second_value, third_key, third_value)
end


if size(spline,1) == size(spline,2)
    @printf("%-35s %2.2f\n", "Increments of", step)
else
    @printf("%-35s %2.2f\n", "Percentiles of", step)
end
print(@sprintf("%-35s", "Additional values (if requested): "))
for value in WHO_search_breaks
    print(@sprintf(" %-4.1f ", value))
end

println()

# Time the block of code using @elapsed and store the output in BIC_opt
search_time, BIC_opt = let
    search_time = @elapsed begin
        BIC_opt_result = estimate_threshold_mat(data, search_mat, K, print_search, N, cum_sum)
    end
    # Calculate hours, minutes, and seconds
    search_time_in_seconds = Int(round(search_time))
    hours, search_time_in_seconds = divrem(search_time_in_seconds, 3600)
    minutes, seconds = divrem(search_time_in_seconds, 60)
    @printf("%-35s %d hours, %d minutes, %d seconds\n", "Search time:", hours, minutes, seconds)
    search_time, BIC_opt_result
end   

@printf("%-35s %d\n", "Max no. of Models (K)",K)

#include BIC_fixed
push!(BIC_opt, vec(BIC_fixed))

#calculate BIC for usual WHO bands and include in BIC_opt
BIC_WHO_gama = BIC_i(data, vec(WHO_search), N, cum_sum)
push!(BIC_opt, vec(BIC_WHO_gama))

#sort the fixed, WHO, 1_opt, 2_opt,...,K_opt and print    
sort!(BIC_opt, by = x -> x[1])
@printf("%-35s\n", "Ranked BIC values with breakpoints")
BIC_opt_gama = BIC_opt

BIC_opt_output = format_BIC_opt(BIC_opt)
println(BIC_opt_output)

X_vars_no_BMI = String.(X_name)

# Define label strings
X_vars_BP = []
X_vars_BP1 = vcat(X_vars_no_BMI, ["spline_1", "spline_2"])
push!(X_vars_BP, X_vars_BP1)

for i in 2:5 #hard coded to max 5 breaks
    X_vars_BPi = vcat(X_vars_BP[i-1], ["spline_$(i+1)"])
    push!(X_vars_BP, X_vars_BPi)
end

# re-estimate optimal model
gama_opt                = BIC_opt[1][2:end]
k_opt_breaks            = size(gama_opt,1)
#spline_names_opt        = Symbol.("spline_" .* string.(1:k_opt_breaks + 1))
spline_names_opt        = Symbol.("Wn_spline_" .* string.(1:k_opt_breaks + 1))
x_splined_opt           = Wn_spline_x(data[:, 1], data[:, 2], vec(gama_opt),cum_sum)
x_splined_opt_df        = DataFrame(x_splined_opt, spline_names_opt)
Wny_name, WnX_opt_names = Wn_names(y_name, X_name, Symbol.("spline_" .* string.(1:k_opt_breaks + 1)))
# Add the columns of x_splined_opt_df to df_FE_big_spec
df_opt = hcat(df_FE_big_spec, x_splined_opt_df)

# finally estimate model
#b_Wn_opt, σ²_opt, SSE_opt, logL_opt, BIC_opt = ols(Wny_name, Symbol.(X_vars_BP[k_opt_breaks]), constant, df_correction, df_opt)
b_Wn_opt, σ²_opt, SSE_opt, logL_opt, BIC_opt = ols(Wny_name, WnX_opt_names, constant, df_correction, df_opt)

# re-estimate WHO model
spline_names_WHO        = Symbol.("Wn_spline_" .* string.(1:size(WHO_search, 1) + 1))
x_splined_WHO           = Wn_spline_x(data[:, 1], data[:, 2], vec(WHO_search), cum_sum)
x_splined_WHO_df        = DataFrame(x_splined_WHO, spline_names_WHO)
Wny_name, WnX_WHO_names = Wn_names(y_name, X_name, Symbol.("spline_" .* string.(1:size(WHO_search, 1) + 1)))

# Add the columns of x_splined_WHO_df to df_FE_big_spec
df_WHO = hcat(df_FE_big_spec, x_splined_WHO_df)

# finally estimate model
#b_Wn_WHO, σ²_WHO, SSE_WHO, logL_WHO, BIC_WHO = ols(Wny_name, Symbol.(X_vars_BP[4]), constant, df_correction, df_WHO)
b_Wn_WHO, σ²_WHO, SSE_WHO, logL_WHO, BIC_WHO = ols(Wny_name, WnX_WHO_names, constant, df_correction, df_WHO)

println()
result_title_fixed = get_result_title("Within Regression Results: Fixed coefficient model", BIC_fixed)
println(result_title_fixed)
fixed_results = getresult_table(b_Wn, SubString.(vcat(X_vars_no_BMI, String(spline_name))))
println(fixed_results)

println()
result_title_WHO = get_result_title("Within Regression Results: WHO model", Matrix(BIC_WHO_gama))
println(result_title_WHO)
WHO_results = getresult_table(b_Wn_WHO, [SubString(x) for x in vec(X_vars_BP[4])])
println(WHO_results)

println()
result_title_opt = get_result_title("Within Regression Results: Optimal model", Matrix(Matrix(hcat(BIC_opt_gama[1,:]...))'))
println(result_title_opt)
opt_results = getresult_table(b_Wn_opt, [SubString(x) for x in vec(X_vars_BP[5])])
println(opt_results)

# write output
output_file = output_file * "K$K.txt"

# Open the output file for writing
output_stream = open(output_file, "w")
println(output_stream, "%-35s\n", "Full sample")
@printf(output_stream, "%-35s\n", "Dstats on estimation dataframe")
DSTATS = print_df_DSTATS(df_FE_big_spec,include_Wns_DSTATS)
println(output_stream, DSTATS)
if select_sample == "yes"
    @printf(output_stream, "%-35s %-6s\n", "Sample has been selected by:", split_sample_name)
end
println(output_stream)  
@printf(output_stream, "%-35s %d\n", "Number of observations: ", size(df_FE_big_spec,1))
@printf(output_stream, "%-35s %d\n", "Max no. of Models (K)",K)
@printf(output_stream,"%-35s %-4d\n", "number of search models: ",size(search_mat,1))
println(output_stream)  
@printf(output_stream, "%-35s\n", "Search Configurations:")
search_time_in_seconds = Int(round(search_time))
hours, search_time_in_seconds = divrem(search_time_in_seconds, 3600)
minutes, seconds = divrem(search_time_in_seconds, 60)
@printf(output_stream, "%-35s %d hours, %d minutes, %d seconds\n", "Search time:", hours, minutes, seconds)
@printf(output_stream, "%-35s %-6s\n", "Spline variable:", spline_name)
@printf(output_stream, "%-35s %-4.1f %-4.1f\n", "Dataframe trunction on above by:", cut_offs[1], cut_offs[2])
@printf(output_stream, "%-35s %-4.1f %-4.1f\n", "Search range on above variable:", search_range[1], search_range[2])
if length(keys_array) == 1
    @printf(output_stream, "%-35s %-6s, %-4.1f\n", "Wn_maximums", first_key, first_value)
elseif length(keys_array) == 2
    @printf(output_stream, "%-35s %-6s, %-4.1f, %-6s, %-4.1f\n", "Wn_maximums", keys_array[1], values_array[1], second_key, second_value)
elseif length(keys_array) == 3
    @printf(output_stream, "%-35s %-6s, %-4.1f, %-6s, %-4.1f, %-6s, %-4.1f\n", "Wn_maximums", keys_array[1], values_array[1], second_key, second_value, third_key, third_value)
end
if size(spline, 1) == size(spline, 2)
    @printf(output_stream, "%-35s %2.2f\n", "Increments of", step)
else
    @printf(output_stream, "%-35s %2.2f\n", "Percentiles of", step)
end
write(output_stream, @sprintf("%-35s", "Additional searches (if requested): "))
for value in WHO_search_breaks
    write(output_stream, @sprintf("%-4.1f", value))
end
write(output_stream, "\n")
println(output_stream)  
println(output_stream, "Optimal BIC and breakpoint values")
println(output_stream, BIC_opt_output)
println(output_stream)  
println(output_stream, result_title_fixed)
println(output_stream, fixed_results)
println(output_stream)  
println(output_stream, result_title_WHO)
println(output_stream, WHO_results)
println(output_stream)  
println(output_stream, result_title_opt)
println(output_stream, opt_results)


# Flush any buffered output to the file stream
flush(output_stream)

# Close the output file
close(output_stream)

#=
#test some latex tables
dist = Normal()
df = rand(dist, (3,2))
df_se = abs.(rand(dist, (3,2)))
p1 = parenthesis("[", ")")
columnlabels = @. "M"*["1", "2"]
rowlabels = @. "X"*["1", "2", "3"]
s = matrix2TeX(df,df_se, rows=rowlabels, columns=columnlabels, file=output_file * "overall.tex")
=#

error("Script execution stopped.")



